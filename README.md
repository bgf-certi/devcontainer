# Development Containers Examples

### What is this repository for?

- This repository shares different examples of how bring-up an dockerized develop environment. By using it, every developer will have the same environment, which ensures coehison between the developers of the team and also with the CI pipeline. Currently have examples for the following environments:
  - C/C++
- [Confluence Page](https://certi-cdm.atlassian.net/wiki/spaces/~613174446/pages/3749151084/Visual+Studio+Code) with the documentation of this repository.

### Contents

- Build from Dockerfile
- Build from docker-compose.yml
- Containers from an existing image (dockerhub, private registry or local built image)

### Contribution guidelines

- Writing tests
- Code review
- Other guidelines -->

### TODO List

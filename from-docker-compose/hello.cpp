#include <iostream>
#include <string>

#ifndef ARCH
#define ARCH "Undefined"
#endif

int main(int argc, char* argv[]) {
  std::cout << "Hello, my architecture is " << argv[1] << std::endl;
  exit(0);
}
